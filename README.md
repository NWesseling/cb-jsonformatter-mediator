# Json formatter mediator

    This mediator handles a couple of issues when using json with the WSO2 ESB / EI. WSO2 is perfectly capable of handling JSON internally however when we convert XML to JSON and vice versa there are a couple of quirks we need to solve.

## Quirks which this mediator solves
    - The standard JSON formatter determines based on the value of a json-key if it needs to apply string-markers in the value. i.e. '{ "key": 1234 } vs { "key": "1234" }'. There is no way to manipulate this using the standard means available
    - When feeding empty XML-elements into the standard jsonformatter then there can happen two things:
        + The XML element is empty or self closing
        + The XML element has an xsi:nil="true" attribute.

    The standard json-formatter will transform an empty xml-element into a key with null construct, i.e. { "key": null }
    If an xsi:nil is present on the XML element then a sub-key is added to the element with the attribute-value on the json-key. i.e. { "key" : { "@nil": true } }

    This is unacceptable in some situations so we need to be able to influence this.
    That's where this mediator steps in....

## Mediator function
    This mediator will inspect the XML for indicators. These are the force-attribute or an xsi:nil attribute and will use these to determine the output-form of the jsonkey.

### force attribute

Example XML:
```
  <root>
    <element force="string">12345</element
  </root>
```

will result in:
```
{
    "element": "12345"
}
```

omit the force attribute to get the numeric representation of the value.

### xsi:nil attribute

Example XML (the element tag can be self-closing or have a separate close-tag):
```
<root>
    <element xsi:nil="true" />
</root>
```

will result in:
```
{
    "element": ""
}
```

### No element
The third hypothetical situation is that you dont want the element to appear at all in the resulting JSON. Simple yet effective; dont include the element in the XML before sending it off into the standard jsonformatter.


## Usage
Usage is simple; Install the mediator in the dropins folder of the ESB and include the following tag AFTER you formatted the proper XLM-representation of a JSON message.

```
<formatJson />
```

The mediator will do its job automatically.