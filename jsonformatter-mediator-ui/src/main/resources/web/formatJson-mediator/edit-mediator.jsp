<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>

<%--
  ~  Copyright (c) 2008, WSO2 Inc. (http://www.wso2.org) All Rights Reserved.
  ~
  ~  Licensed under the Apache License, Version 2.0 (the "License");
  ~  you may not use this file except in compliance with the License.
  ~  You may obtain a copy of the License at
  ~
  ~        http://www.apache.org/licenses/LICENSE-2.0
  ~
  ~  Unless required by applicable law or agreed to in writing, software
  ~  distributed under the License is distributed on an "AS IS" BASIS,
  ~  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  ~  See the License for the specific language governing permissions and
  ~  limitations under the License.
  --%>

<%@ page import="com.yenlo.wso2.mediators.ui.JsonFormatterMediator" %>
<%@ page import="org.wso2.carbon.mediator.service.ui.Mediator" %>
<%@ page import="org.wso2.carbon.sequences.ui.util.SequenceEditorHelper" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://wso2.org/projects/carbon/taglibs/carbontags.jar" prefix="carbon" %>

<script language="JavaScript">
    // Mandatory validate function
    function formatJsonMediatorValidate() {
        return true;
    }
</script>

<%
    Mediator mediator = SequenceEditorHelper.getEditingMediator(request, session);

    String inputValue = "";

    if (!(mediator instanceof JsonFormatterMediator)) {
        // todo : proper error handling
        throw new RuntimeException("Unable to edit the formatJson mediator");
    }
    JsonFormatterMediator myMediator = (JsonFormatterMediator) mediator;

    if (myMediator.getValue() != null) {
        inputValue = myMediator.getValue();
    }

    /**
     * Here you should read the values from your mediator-instance (myMediator) and put them in field of the JSP.
     * This makes referencing them easier and you have all supported values in one place defined... Namely here :)
     *
     * i.e.:
     *     String attributeValue = "";
     *     if(myMediator.getAttributeValue() != null){
     *      attributeValue = myMediator.getAttributeValue();
     *      }
     */

%>

<fmt:bundle basename="com.yenlo.wso2.mediators.ui.i18n.Resources">
    <div>
        <table class="normal" width="100%">
            <tbody>
            <tr>
                <td colspan="2">
                    <h2><fmt:message key="formatJson.mediator"/></h2>
                </td>
            </tr>
            <tr>
                <td>
                    <fmt:message key="value.header"/>
                </td>
                <td>
                    <input type="text" name="myvalue" id="myvalue" value="<%=inputValue%>"/>
                </td>
            </tr>

            <!-- Enumerate all supported attributes here.

                 Create two rows per attribute. The first row shows the attributename, the second the input or dropdown
                 or some other input-type to allow user-input as the specified value for the attribute.

                i.e.:
                <tr>
                    <td colspan="2">
                        <h3 class="mediator">
                            <fmt:message key="mode.header"/>
                        </h3>
                    </td>
                </tr>

                <tr>
                    <td>
                        <fmt:message key="mode"/><span class="required">*</span>
                    </td>
                    <td>
                        <select name="stackmode" id="stackmode">
                            <option <% //=mode.equalsIgnoreCase("Push")?"selected=\"selected\"":""%>>Push</option>
                            <option <% //=mode.equalsIgnoreCase("Peek")?"selected=\"selected\"":""%>>Peek</option>
                            <option <% //=mode.equalsIgnoreCase("Pop")?"selected=\"selected\"":""%>>Pop</option>
                        </select>
                    </td>
                </tr>
            -->

        </table>
    </div>
</fmt:bundle>
