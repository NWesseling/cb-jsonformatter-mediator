package nl.consumentenbond.wso2.mediators.ui;

import org.wso2.carbon.mediator.service.AbstractMediatorService;
import org.wso2.carbon.mediator.service.ui.Mediator;

/**
 * @author thijs.volders@yenlo.com
 * Date: 10/09/14
 */
public class JsonFormatterMediatorService extends AbstractMediatorService {

    public String getTagLocalName() {
        return JsonFormatterMediator.MEDIATOR_ELEMENT_NAME;
    }

    /**
     * This value is used to display in the ui-design view
     */
    public String getDisplayName() {
        return JsonFormatterMediator.MEDIATOR_ELEMENT_NAME;
    }

    public String getLogicalName() {
        return "JsonFormatterMediatorMediator";
    }

    public String getGroupName() {
        return "Extension";
    }

    public Mediator getMediator() {
        return new JsonFormatterMediator();
    }
}
