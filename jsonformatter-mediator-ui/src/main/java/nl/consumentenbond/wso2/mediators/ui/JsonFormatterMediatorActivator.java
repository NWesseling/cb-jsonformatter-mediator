package nl.consumentenbond.wso2.mediators.ui;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.wso2.carbon.mediator.service.MediatorService;

import java.util.Hashtable;

/**
 * @author thijs.volders@yenlo.com
 * Date: 10/09/14
 */
public class JsonFormatterMediatorActivator implements BundleActivator {

    private static final Log log = LogFactory.getLog(JsonFormatterMediatorActivator.class);

    public void start(BundleContext bundleContext) {

        if (log.isDebugEnabled()) {
            log.debug("Starting the formatJson mediator component ...");
        }

        Hashtable props = new Hashtable();
        bundleContext.registerService(MediatorService.class.getName(), new JsonFormatterMediatorService(), props);

        if (log.isDebugEnabled()) {
            log.debug("Successfully registered the formatJson mediator service");
        }
    }

    public void stop(BundleContext bundleContext) {
        if (log.isDebugEnabled()) {
            log.debug("Stopped the formatJson mediator component ...");
        }
    }
}
