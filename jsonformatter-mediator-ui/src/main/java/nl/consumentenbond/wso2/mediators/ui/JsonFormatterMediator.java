package nl.consumentenbond.wso2.mediators.ui;

import org.apache.axiom.om.OMAttribute;
import org.apache.axiom.om.OMElement;
import org.apache.synapse.SynapseConstants;
import org.apache.synapse.config.xml.XMLConfigConstants;
import org.wso2.carbon.mediator.service.ui.AbstractMediator;

import javax.xml.namespace.QName;

/**
 * @author thijs.volders@yenlo.com
 * Date: 10/09/14
 */
public class JsonFormatterMediator extends AbstractMediator {

    public static final String MEDIATOR_ELEMENT_NAME = "formatJson";

    /*
    * Enumerate all instance variables (i.e. the supported attributes of your mediator here..
    *
    * i.e.
    * private String mode;
    * private String xpath;
    */

    public String getTagLocalName() {
        return MEDIATOR_ELEMENT_NAME;
    }

    /**
     * This method should serialize 'this' instance into an XML form.
     * <p>
     * 	The XML form of this mediator is (as) identical (as possible) to the XML-structure that the developer
     *  would create when adding this mediator in the 'Source view' of the proxy.
     * </p>
     * <p> 
     *  Here you output the element itself, add any attributes or child-element which you support with their 
     *  corresponding values. These values are stored as instance variables in this class. 
     *  You should assume one instancevariable per attribute.
     * </p>
     *
     */
    public OMElement serialize(OMElement parent) {
	    // Create the mediator's XML element first
        OMElement mediatorElement = fac.createOMElement(MEDIATOR_ELEMENT_NAME , synNS);
        saveTracingState(mediatorElement, this);

    	// Add the element to the parent.
        if (parent != null) {
            parent.addChild(mediatorElement);
        }

        return mediatorElement;
    }

    /**
     * Use the incoming omElement to fill 'this' instance with values.
     * <p>
     *  Get the attributevalues and child-contents of the incoming omElement and persist these values
     *  in this instance. The serialize method above may be called to serialize these values back to XML form.
     * </p>
     *
     * @param omElement the XML-representation of the mediator's configuration (as used in proxy services)
     */
    public void build(OMElement omElement) {
        processAuditStatus(this, omElement);
    }
}
