package nl.consumentenbond.wso2.mediators;

import org.apache.axiom.om.OMElement;
import org.apache.axis2.AxisFault;
import org.apache.synapse.commons.json.JsonUtil;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import java.util.Iterator;

public class JsonMessage {
    private StringBuilder message;
    
    public String format(OMElement root) throws AxisFault {
    	
        try {
            StringBuilder sb = JsonUtil.toJsonString(root);

            String rootElement = sb.toString();
            if (rootElement.startsWith("[")) {
                // JsonArray
                JSONArray ar = new JSONArray(rootElement);
                JSONArray newAr = fixJSONArray(ar);

                return newAr.toString();
            } else if (rootElement.startsWith("{")) {
                JSONObject obj = new JSONObject(rootElement);
                JSONObject newObj = fixJSONObject(obj);

                return newObj.toString();
            }
        } catch (AxisFault | JSONException je) {
            throw new AxisFault("There was an issue processing the message", je);
        }

        return null;
    }

    private JSONArray fixJSONArray(JSONArray jsonArray) throws JSONException {
        JSONArray result = new JSONArray();
        for (int i=0;i<jsonArray.length(); i++) {
            Object obj = jsonArray.get(i);
            if (obj instanceof  JSONObject) {
                result.put(i, fixJSONObject((JSONObject)obj));
            } else if (obj instanceof JSONArray) {
                result.put(i, fixJSONArray((JSONArray) obj));
            } else {
                result.put(i, obj.toString());
            }
        }
        return result;
    }

    private JSONObject fixJSONObject(JSONObject jsonObject) throws JSONException {
        JSONObject result = new JSONObject();

        for (Iterator<String> iterator= jsonObject.keys(); iterator.hasNext();) {
            String key = iterator.next();
            Object value = jsonObject.get(key);

            if (value instanceof JSONObject) {
                JSONObject jObj = (JSONObject)value;
                                
                if (jObj.has("@force")) {
                	if (jObj.get("@force").toString().equalsIgnoreCase("string")) {               
	                    if (jObj.has("$")) {
	                        result.put(key, jObj.get("$").toString());
	                    } else {
	                        result.put(key, JSONObject.NULL);
	                    }
                	}
                	else if (jObj.get("@force").toString().equalsIgnoreCase("empty")) { 
                		result.put(key, "");
                	}
                	else if (jObj.get("@force").toString().equalsIgnoreCase("array")) { 

                		for (Iterator<String> jObjIt = jObj.keys(); jObjIt.hasNext();) {
                			String jObjKey = jObjIt.next();	                			
                			
                			if (!"@force".equals(jObjKey)) {
                				
                				
                				if (jObj.get(jObjKey) instanceof JSONArray) {
                					JSONArray teConverterenArray = (JSONArray) jObj.get(jObjKey);
                					
                					//TODO betere naamgeving
                					JSONArray geconverteerd = new JSONArray();
                					
                					for (int i = 0; i < teConverterenArray.length(); i++) {
                						JSONObject element = new JSONObject("{\"" + jObjKey + "\":\"" +  teConverterenArray.get(i).toString() + "\"}");
                						geconverteerd.put(element);
                					}
                					
                					result.put(key, geconverteerd);
                				}
                				else {
                					JSONObject element = new JSONObject("{\"" + jObjKey + "\":\"" +  jObj.get(jObjKey).toString() + "\"}");
	                				JSONArray jsonArray = new JSONArray();
	                				jsonArray.put(element);
	                				result.put(key, jsonArray);
                					
                				}
                				break;
                			}
                		}
            		}                			
                	
                } else if (jObj.has("@nil")) {
                    if (jObj.get("@nil").toString().equalsIgnoreCase("true")) {
                        // When nil was true, set a NULL object.
                        result.put(key, JSONObject.NULL);
                    } else if (!jObj.has("$")) {
                        // Otherwise, if there is no value then set an empty string as there is no nil indicator.
                        result.put(key, "");
                    }
                } else {
                    result.put(key, fixJSONObject(jObj));
                }
            } else if (value instanceof JSONArray) {
                result.put(key, fixJSONArray((JSONArray) value));
            } else {
                if (value.equals(JSONObject.NULL)) {
                    result.put(key, "");
                } else {
                    result.put(key, value);
                }
            }
        }

        return result;
    }
}
