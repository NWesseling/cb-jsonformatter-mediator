package nl.consumentenbond.wso2.mediators.xml;

import nl.consumentenbond.wso2.mediators.JsonFormatterMediator;
import org.apache.axiom.om.OMElement;
import org.apache.synapse.Mediator;
import org.apache.synapse.SynapseConstants;
import org.apache.axiom.om.*;
import org.apache.synapse.config.xml.AbstractMediatorSerializer;

/**
 * @author thijs.volders@yenlo.com
 * Date: 10/09/14
 */
public class JsonFormatterMediatorSerializer extends AbstractMediatorSerializer {
    protected static final OMFactory fac = OMAbstractFactory.getOMFactory();
    protected static final OMNamespace synNS = SynapseConstants.SYNAPSE_OMNAMESPACE;

    public OMElement serializeSpecificMediator(Mediator mediator) {
        assert mediator instanceof JsonFormatterMediator : "JsonFormatterMediator mediator is expected";
        return fac.createOMElement(JsonFormatterMediator.MEDIATOR_ELEMENT_NAME, synNS);
    }

    public String getMediatorClassName() {
        return JsonFormatterMediator.class.getName();
    }
}
