package nl.consumentenbond.wso2.mediators;


import org.apache.axiom.om.OMElement;
import org.apache.axiom.soap.SOAPEnvelope;
import org.apache.axis2.AxisFault;
import org.apache.axis2.description.AxisService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.synapse.MessageContext;
import org.apache.synapse.SynapseLog;
import org.apache.synapse.commons.json.JsonUtil;
import org.apache.synapse.core.axis2.Axis2MessageContext;
import org.apache.synapse.mediators.AbstractMediator;

/**
 * This mediator was written with the use of blog-post -&gt; http://maninda.blogspot.nl/2012/11/writing-custom-mediator-for-wso2-esb.html
 * <p>
 * Its an evolution of the JsonFormatterMediator written by Tobias Groothuyse.
 * </p>
 * <p>
 * This mediator allows an easy way to stack the messageContext for later use.
 * Please be aware that attachments from the current messageContext are not stacked!!
 * </p>
 *
 * @author thijs.volders@yenlo.com
 * Date: 10/09/14
 */
public class JsonFormatterMediator extends AbstractMediator {
    private static final Log LOG = LogFactory.getLog(JsonFormatterMediator.class);

    public static final String MEDIATOR_ELEMENT_NAME = "formatJson";

    private static final String ADMIN_SERVICE_PARAMETER = "adminService";
    private static final String HIDDEN_SERVICE_PARAMETER = "hiddenService";

	//
    /**
     * is this mediator content Aware? i.e. does it need the message in the envelop? If Yes then return true, otherwise return false
     */
    public boolean isContentAware() {
        return false;
    }

    /**
     * This method contains the logic for this mediator implementation
     *
     * @param messageContext The active messageContext is supplied through this parameter
     */
    public boolean mediate(MessageContext messageContext) {

        SynapseLog synLog = getLog(messageContext);

	    /* Default mediator logging */
        if (synLog.isTraceOrDebugEnabled()) {
            synLog.traceOrDebug("Start : JsonFormatterMediator mediator");
        }

        // The Axis2 messageContext is extracted here.
        org.apache.axis2.context.MessageContext msgCtx = ((Axis2MessageContext) messageContext).getAxis2MessageContext();


	    // This is some boilerplate code. If you're not interested in the Axis2Service instance then remove below lines
        AxisService service = msgCtx.getAxisService();
        if (service == null) {
            return true;
        }

        // When this is not inside an API these parameters should be there
        if ((!service.getName().equals("__SynapseService")) &&
                (service.getParameter(ADMIN_SERVICE_PARAMETER) != null ||
                        service.getParameter(HIDDEN_SERVICE_PARAMETER) != null)) {
            return true;
        }

        try {
            SOAPEnvelope envelope = msgCtx.getEnvelope();
            OMElement firstElement = envelope.getBody().getFirstElement();
            String formatted = new JsonMessage().format(firstElement);
            if (formatted != null) {
                JsonUtil.newJsonPayload(msgCtx, formatted, true, true);
            } else {
                synLog.traceOrDebug("Cannot process payload. Skipping formatter.");
                return true;
            }
        } catch (AxisFault af) {
            synLog.error(af);
            return false;
        }

        /* Default mediator logging */
        if (synLog.isTraceOrDebugEnabled()) {
            synLog.traceOrDebug("End : JsonFormatterMediator mediator");
        }

        return true;
    }
}
