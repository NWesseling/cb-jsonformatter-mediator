package nl.consumentenbond.wso2.mediators.xml;

import nl.consumentenbond.wso2.mediators.JsonFormatterMediator;
import org.apache.axiom.om.OMElement;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.synapse.Mediator;
import org.apache.synapse.SynapseConstants;
import org.apache.synapse.config.xml.AbstractMediatorFactory;

import javax.xml.namespace.QName;
import java.util.Properties;

/**
 * @author thijs.volders@yenlo.com
 * Date: 10/09/14
 */
public class JsonFormatterMediatorFactory extends AbstractMediatorFactory {
    private static final Log log = LogFactory.getLog(JsonFormatterMediatorFactory.class);
    public static final QName MEDIATER_Q = new QName(SynapseConstants.SYNAPSE_NAMESPACE, JsonFormatterMediator.MEDIATOR_ELEMENT_NAME);

    public Mediator createSpecificMediator(OMElement omElement, Properties properties) {
        log.debug("Creating the JsonFormatterMediator mediator instance");
        return new JsonFormatterMediator();
    }

    @Override
    public QName getTagQName() {
        return MEDIATER_Q;
    }
}
