package nl.consumentenbond.wso2.mediators;

import org.apache.axiom.om.OMXMLBuilderFactory;
import org.apache.axiom.om.OMXMLParserWrapper;
import org.testng.annotations.Test;
import java.io.InputStream;

public class JsonMessageTest {

    /**
     * Very simple test which actually does not validate anything :8)
     * @throws Exception
     */
    @Test
    public void testFormat() throws Exception {
        //read a file from disk, feed it into the JsonMessage and see the output
        InputStream resourceAsStream = this.getClass().getResourceAsStream("/testfile1.xml");
        OMXMLParserWrapper omBuilder = OMXMLBuilderFactory.createOMBuilder(resourceAsStream);

        String format = new JsonMessage().format(omBuilder.getDocumentElement());
        System.out.println(format);
    }
}